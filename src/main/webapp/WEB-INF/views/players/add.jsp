<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Создать игрока</title>
</head>
<body>

<form:form action="/players/add" method="post" modelAttribute="players">
    <table>
        <tr>
            <td><form:label path="name">Имя</form:label></td>
            <td><form:input path="name"/></td>
            <td><form:errors path="name"/></td>
        </tr>
        <tr>
            <td><form:label path="surname">Фамилия</form:label></td>
            <td><form:input path="surname"/></td>
            <td><form:errors path="surname"/></td>
        </tr>
        <tr>
            <td><form:label path="performance">Показатель</form:label></td>
            <td><form:input path="performance"/></td>
            <td><form:errors path="performance"/></td>
        </tr>
        <tr>
            <td><form:label path="price">Цена</form:label></td>
            <td><form:input path="price"/></td>
            <td><form:errors path="price"/></td>
        </tr>
    </table>
    <input type="submit" value="save"/>
</form:form>

</body>
</html>
