<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Добавить пост</title>
</head>
<body>

<form:form action="/players/${player.id}/save" method="post" modelAttribute="players">
    <table>
        <tr>
            <td><form:label path="title">Имя</form:label></td>
            <td><form:input path="title"/></td>
            <td><form:errors path="title"/></td>
        </tr>
        <tr>
            <td><form:label path="title">Фамилия</form:label></td>
            <td><form:input path="title"/></td>
            <td><form:errors path="title"/></td>
        </tr>
        <tr>
            <td><form:label path="text">Потенциал</form:label></td>
            <td><form:input path="text"/></td>
            <td><form:errors path="text"/></td>
        </tr>
        <tr>
            <td><form:label path="text">Цена</form:label></td>
            <td><form:input path="text"/></td>
            <td><form:errors path="text"/></td>
        </tr>
    </table>
    <input type="submit" value="save"/>
</form:form>

</body>
</html>
