<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список игроков</title>
</head>
<body>
<h1>Список игроков</h1>


<sec:authorize ifAnyGranted="ROLE_ADMIN, ROLE_USER">

    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <p>Здорова, босс!!</p>
        <a href="/players/add">Создать игрока</a>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
        <a href="/">Главная страница</a>
    </sec:authorize>
    <c:if test="${players.size() == 0}">
        <p>Пока ничего нету</p>
    </c:if>
    <table border="1">
        <tr>
            <td><small>Имя</small></td>
            <td><small>Фамилия</small></td>
            <td><small>Потенциал</small></td>
            <td><small>Цена</small></td>
            <td><small>Команда</small></td>
            <td><small>Продан</small></td>
        </tr>
        <c:forEach var="player" items="${players}">
                <tr>
                    <td><middle>${player.name}</middle></td>
                    <td><middle>${player.surname}</middle></td>
                    <td><middle>${player.performance}</middle></td>
                    <td><middle>${player.price}</middle></td>
                    <td><middle>${player.team.name}</middle></td>
                    <c:set var="isBought" value="нет"/>
                    <c:if test="${player.bought==true}"> <c:set var="isBought" value="да"/> </c:if>
                    <td><middle>${isBought}</middle></td>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <form:form action="/players/${player.id}/buy" method="post">
                            <c:if test="${player.bought==false}">
                                <td> <input type="submit" value="купить"/></td>
                            </c:if>
                        </form:form>
                    </sec:authorize>
                </tr>
        </c:forEach>
    </table>
</sec:authorize>
<security:authorize ifNotGranted="ROLE_USER, ROLE_ADMIN">
    <p>Ну ка залогиньтесь!</p>
    <p><a href="/login">Ок</a></p>
</security:authorize>


</body>
</html>
