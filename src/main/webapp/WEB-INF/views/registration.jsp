<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Регистрация</title>
</head>
<body>

<form:form action="/registration" method="post" modelAttribute="userForm">
    <table>
        <tr>
            <td><form:label path="email">Логин</form:label></td>
            <td><form:input path="email"/></td>
            <td><form:errors path="email"/></td>
        </tr>
        <%--<tr>
            <td><form:label path="password">Пароль</form:label></td>
            <td><form:input type = "password" path="password"/></td>
            <td><form:errors path="password"/></td>
        </tr>--%>
        <tr>
            <td><form:label path="name">Имя</form:label></td>
            <td><form:input path="name"/></td>
            <td><form:errors path="name"/></td>
        </tr>
        <tr>
            <td><form:label path="surname">Фамилия</form:label></td>
            <td><form:input path="surname"/></td>
            <td><form:errors path="surname"/></td>
        </tr>
        <tr>
            <td><form:label path="password">Пароль</form:label></td>
            <td><form:password path="password"/></td>
            <td><form:errors path="password"/></td>
        </tr>
    </table>
    <input type="submit" value="save"/>
</form:form>
</body>
</html>
