<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/css/main.css">
</head>
<body>

   <a href="/logout" class="btn btn-info pull-right " role="button">Выйти</a>
   <nav class="navbar navbar-default">
       <div class="collapse navbar-collapse" id="main-menu" >
           <ul class="nav navbar-nav navbar-right">
               <a href="/logout" class="btn btn-info pull-right " role="button">Выйти</a>
           </ul>
       </div>
   </nav>

</body>
</html>
