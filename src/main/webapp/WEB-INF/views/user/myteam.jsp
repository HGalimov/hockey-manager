<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Моя команда</title>
</head>
<body>


<sec:authorize ifAnyGranted="ROLE_USER">
    <a href="/">Главная страница</a>
    <c:if test="${myteam == null}">
        <p>Команды нет</p>
    </c:if>
    <c:if test="${myteam != null}">
        <table border="1">
            <tr>
                <td><small>Название</small></td>
                <td><small>Игроки</small></td>
            </tr>
            <tr>
                <c:if test="${myteam.id ne 0}">
                   <td><middle>${myteam.name}</middle></td>
                    <c:forEach var="player" items="${players}">
                        <td><middle>${player.name} ${player.surname}</middle></td>
                    </c:forEach>
                </c:if>

            </tr>
        </table>
    </c:if>
</sec:authorize>


</body>
</html>
