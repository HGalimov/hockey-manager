<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Создание команды</title>
</head>
<body>

<form:form action="/create" method="post" modelAttribute="teamForm">
    <form:label path="name">Название</form:label>
    <form:input path="name"/>
    <form:errors path="name"/>
    <input type="submit" value="Сохранить"/>
</form:form>

</body>
</html>
