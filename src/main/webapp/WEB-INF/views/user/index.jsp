<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Главная страница</title>
    <%@ include file="../header.jsp" %>
</head>
<body>
<h1>Добро пожаловать</h1>


        <sec:authorize ifAnyGranted="ROLE_USER">
    <p>Здорова, менеджер!!</p>
    <p>${error}</p>
    <a href="/players/index">Список игроков</a>
    <a href="/teams/index">Список команд</a>
    <a href="/create">Создать команду</a>
    <a href="/myteam">Моя команда</a>
</sec:authorize>

</body>
</html>
