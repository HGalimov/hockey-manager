<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список команд</title>
</head>
<body>

<sec:authorize access="hasRole('ROLE_USER')">
    <a href="/">Главная страница</a>
</sec:authorize>

<sec:authorize ifAnyGranted="ROLE_ADMIN, ROLE_USER">
    <c:if test="${teams.size() == 0}">
        <p>Список пуст</p>
    </c:if>
    <table border="1">
        <tr>
            <td><small>Название</small></td>
        </tr>
        <c:forEach var="team" items="${teams}">
            <tr>
                <c:if test="${team.id ne 0}">
                    <td><middle>${team.name}</middle></td>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <c:if test="${(team.id ne user.team.id) && (user.team != null)}">
                            <form:form action="/teams/${team.id}/play" method="post">
                                <td><input type="submit" value="играть"/></td>
                            </form:form>
                        </c:if>
                    </sec:authorize>
                </c:if>
            </tr>
        </c:forEach>
    </table>
</sec:authorize>


</body>
</html>
