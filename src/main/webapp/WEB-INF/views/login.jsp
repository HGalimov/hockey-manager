<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Логин</title>
</head>
<body>
<c:if test="${error != null}">
    <p>Вы что-то ввели не так!</p>
</c:if>
<form action="/login/process" method="post">
    Логин: <input type="text" name="login"> <br>
    Пароль <input type="password" name="password"> <br>
    Запомнить? <input type="checkbox" name="remember"> <br>
    <input type="submit">
</form>
<p><a href="/registration">Зарегистрироваться</a></p>
</body>
</html>
