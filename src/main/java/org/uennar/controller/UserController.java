package org.uennar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Player;
import org.uennar.model.Team;
import org.uennar.model.User;
import org.uennar.repository.TeamRepository;
import org.uennar.service.TeamService;
import org.uennar.util.form.TeamForm;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
public class UserController {

    @Autowired
    TeamService teamService;

    @Autowired
    TeamRepository teamRepository;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String registration(ModelMap map) {
        if (SecurityUtils.getCurrentUser().getTeam() == null) {
            map.addAttribute("teamForm", new TeamForm());
            return "user/team";
        }
        map.addAttribute("error", "Вы команду уже создали");
        return "user/index";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("teamForm") @Valid TeamForm form, BindingResult result) {
        //userValidator.validate(form, result);
        if (result.hasErrors()) {
            return "user/team";
        }
        teamService.saveTeam(form);
        return "user/index";
    }

    @RequestMapping(value = "/myteam", method = RequestMethod.GET)
    public String myTeam(ModelMap map) {
        User user = SecurityUtils.getCurrentUser();
        Team myTeam = user.getTeam();

        if (myTeam != null) {
            map.addAttribute("myteam", myTeam);
            Set<Player> players = teamRepository.findPlayersByTeam(myTeam);
            if (players != null) {
                map.addAttribute("players", players);
            }
        }
        return "user/myteam";
    }


}
