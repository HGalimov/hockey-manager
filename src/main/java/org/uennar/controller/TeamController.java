package org.uennar.controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Player;
import org.uennar.model.Team;
import org.uennar.model.User;
import org.uennar.repository.TeamRepository;
import org.uennar.service.PlayService;

import java.util.List;
import java.util.Random;
import java.util.Set;

@Controller
@RequestMapping("/teams")
public class TeamController {

    //


    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayService playService;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String teamsPage(ModelMap map) {
        map.addAttribute("teams", teamRepository.findAll());
        map.addAttribute("user", SecurityUtils.getCurrentUser());
        return "teams/index";
    }

    @Transactional
    @RequestMapping(value = "/{id}/play", method = RequestMethod.POST)
    public String play(ModelMap map, @PathVariable("id") Long id) {
        User user = SecurityUtils.getCurrentUser();
        Team myTeam = user.getTeam();
        Team rival = teamRepository.getOne(id);
        String result = playService.play(rival, myTeam);
        map.addAttribute("result", result);
        return "user/result";
    }
}
