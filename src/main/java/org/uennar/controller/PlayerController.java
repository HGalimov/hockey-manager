package org.uennar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Player;
import org.uennar.model.Team;
import org.uennar.model.User;
import org.uennar.repository.PlayerRepository;
import org.uennar.repository.UserRepository;
import org.uennar.repository.TeamRepository;
import org.uennar.service.PlayerService;
import org.uennar.util.form.PlayerForm;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/players")
public class PlayerController {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    PlayerService playerService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TeamRepository teamRepository;

   /* @RequestMapping
    public String getAllPosts(@RequestParam(value = "phrase", required = false) String phrase, ModelMap map) {
        List<Post> posts = postRepository.findAll();
        if (phrase != null) {
            posts = posts.stream().filter(p -> p.getText().contains(phrase)).collect(Collectors.toList());
        }
        map.addAttribute("posts", posts);
        return "posts/index";
    }
*/
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addNewPlayerPage(ModelMap map) {
        map.addAttribute("players", new Player());
        return "players/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addNewPost(@ModelAttribute @Valid PlayerForm form, BindingResult result) {
        if (result.hasErrors()) {
            return "players/add";
        }
        playerService.savePlayer(form);
        return "redirect:/";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showPost(@PathVariable("id") Long id, ModelMap map) throws Exception {
        Player player = playerRepository.findOne(id);
        if (player == null) {
            throw new Exception("not found");
        }
        map.addAttribute("players", player);
        return "/players/show";
    }

    /*@Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public String deletePost(@PathVariable("id") Long id) {
        Post post = postRepository.findOne(id);
        if (!userCanEditPost(post)) {
            throw new AccessDeniedException("Acces denied");
        }
        postRepository.delete(post);
        return "redirect:/posts";
    }*/

    //    @PostAuthorize("isAuthorized() and #map['post'].user.id eq principal.id")
    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public String editPlayer(@PathVariable("id") Long id, ModelMap map) {
        Player player = playerRepository.findOne(id);
       /* if (!userCanEditPost(post)) {
            throw new AccessDeniedException("Acces denied");
        }*/
        map.addAttribute("players", player);
        return "players/edit";
    }

    @RequestMapping(value = "/{id}/save", method = RequestMethod.POST)
    public String savePlayer(@ModelAttribute @Valid PlayerForm form, BindingResult result) {
        if (result.hasErrors()) {
            return "players/edit";
        }
        playerService.savePlayer(form);


        return "redirect:/";
    }
    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String index(ModelMap map) {
        List<Player> list = playerRepository.findAll();
        map.addAttribute("players", list);
        return "players/index";
    }
    @RequestMapping(value = "/{id}/buy", method = RequestMethod.POST)
    public String buyPlayer(@PathVariable("id") Long id) {
        Player player = playerRepository.findOne(id);
        if (player != null) {
            Team team = SecurityUtils.getCurrentUser().getTeam();
            if (team != null) {
                player.setTeam(team);
                player.setBought(true);
             //   team = teamRepository.getOne(team.getId());
                Set<Player> players = team.getPlayers();
                if(players == null) {
                    players = new HashSet<>();
                }
                players.add(player);
                team.setPlayers(players);
                playerRepository.save(player);
            }
        }
        return "redirect:/players/index";
    }

}
