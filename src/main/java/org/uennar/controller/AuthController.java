package org.uennar.controller;

import org.uennar.config.SecurityUtils;
import org.uennar.model.User;
import org.uennar.service.UserService;
import org.uennar.util.form.UserForm;
//import inno.util.validators.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);



    @Autowired
    UserService userService;

    //@Autowired
   // UserValidator userValidator;

    @RequestMapping("/login")
    public String loginPage(@RequestParam(value = "error", required = false) Boolean error, ModelMap map) {

        if (error == Boolean.TRUE) {
            map.addAttribute("error", true);
        }
        return "login";
    }
   /* @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        if (SecurityUtils.hasRole("ROLE_USER")) {
            return "user/index";
        }
        if (SecurityUtils.hasRole("ROLE_ADMIN")) {

        }
        return "login";
    }*/

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(ModelMap map) {
        map.addAttribute("userForm", new UserForm());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("userForm") @Valid UserForm form, BindingResult result) {
        //userValidator.validate(form, result);
        if (result.hasErrors()) {
            return "registration";
        }
        userService.saveUser(form);
        return "redirect:/login";
    }


    @RequestMapping("/")
    public String index() {
        if (SecurityUtils.hasRole("ROLE_ADMIN")) {
            return "redirect:/players/index";
        }
        if (SecurityUtils.hasRole("ROLE_USER")) {
            return "user/index";
        }
        return "redirect:/login";
    }


}
