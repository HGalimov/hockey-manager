package org.uennar.util.transformers;

import org.springframework.stereotype.Component;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Team;
import org.uennar.model.User;
import org.uennar.util.form.TeamForm;

@Component
public class TeamFormTransformer {

    public Team toTeam(TeamForm form) {
        Team team = new Team();
        team.setName(form.getName());
        User user = SecurityUtils.getCurrentUser();
        team.setUser(user);
        user.setTeam(team);
        return team;
    }
}
