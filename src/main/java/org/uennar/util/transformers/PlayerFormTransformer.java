package org.uennar.util.transformers;

import org.springframework.stereotype.Component;
import org.uennar.model.Player;
import org.uennar.util.form.PlayerForm;

@Component
public class PlayerFormTransformer {

    public Player toPlayer(PlayerForm form) {
        Player player = new Player();
        player.setName(form.getName());
        player.setSurname(form.getSurname());
        player.setPrice(form.getPrice());
        player.setPerformance(form.getPerformance());
        return player;
    }
}
