package org.uennar.model;

//import javax.persistence.*;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Entity
/*@SequenceGenerator(sequenceName = "player_seq", name = "playerSequence")*/
public class Player {

    @Id/*
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "playerSequence")*/
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    private Integer performance;
    private Integer price;
    @Column(nullable = false)
    private Boolean isBought;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;


    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getPerformance() {
        return performance;
    }

    public void setPerformance(Integer performance) {
        this.performance = performance;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getBought() {
        return isBought;
    }

    public void setBought(Boolean bought) {
        isBought = bought;
    }

}
