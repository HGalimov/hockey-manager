package org.uennar.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.uennar.model.Player;
import org.uennar.model.Team;

import java.util.List;
import java.util.Set;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    @Query("select p from Player p where p.team = :team")
    Set<Player> findPlayersByTeam(@Param("team") Team team);
}
