package org.uennar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.uennar.model.Player;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
        Player findByName(String name);
}
