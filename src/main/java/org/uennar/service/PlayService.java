package org.uennar.service;


import org.uennar.model.Team;

public interface PlayService {
    String play(Team rival, Team myTeam);
}
