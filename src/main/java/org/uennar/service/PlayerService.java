package org.uennar.service;

import org.uennar.util.form.PlayerForm;

public interface PlayerService {
    void savePlayer(PlayerForm form);
}
