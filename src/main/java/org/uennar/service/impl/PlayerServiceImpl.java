package org.uennar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Player;
import org.uennar.model.User;
import org.uennar.repository.TeamRepository;
import org.uennar.repository.UserRepository;
import org.uennar.service.PlayerService;
import org.uennar.util.form.PlayerForm;
import org.uennar.util.transformers.PlayerFormTransformer;
import org.uennar.repository.PlayerRepository;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService{
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    PlayerFormTransformer transformer;
    @Override
    public void savePlayer(PlayerForm form) {
        Player player = transformer.toPlayer(form);
        player.setBought(false);
        player.setTeam(teamRepository.findOne(0L));
        playerRepository.save(player);
    }
}
