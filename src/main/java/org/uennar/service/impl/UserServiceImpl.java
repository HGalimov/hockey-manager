package org.uennar.service.impl;

import org.uennar.model.Role;
import org.uennar.model.User;
import org.uennar.repository.RoleRepository;
import org.uennar.repository.UserRepository;
import org.uennar.service.UserService;
import org.uennar.util.form.UserForm;
import org.uennar.util.transformers.UserFormTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final String DEFAULT_ROLE_NAME = "ROLE_USER";
    private Role defaultRole;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserFormTransformer transformer;

    @PostConstruct
    private void initialize() {
        defaultRole = roleRepository.findByName(DEFAULT_ROLE_NAME);
    }

    @Transactional
    @Override
    public void saveUser(UserForm form) {
        User user = transformer.toUser(form);
        user.getRoles().add(defaultRole);
        userRepository.save(user);
    }
}
