package org.uennar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Player;
import org.uennar.model.Team;
import org.uennar.model.User;
import org.uennar.repository.TeamRepository;
import org.uennar.service.PlayService;

import java.util.Random;
import java.util.Set;

@Service
@Transactional
public class PlayServiceImpl implements PlayService {

    @Autowired
    TeamRepository teamRepository;

    @Override
    public String play(Team rival, Team myTeam) {
        String result = "";
        if (myTeam != null) {
            Integer rivalPower = calculatePower(rival);
            Integer myTeamPower = calculatePower(myTeam);
            if (rivalPower != 0 && myTeamPower != 0) {
                Integer sum = rivalPower + myTeamPower;
                Random random = new Random();
                Integer randomNumber = random.nextInt(sum) + 1;

                Integer min = rivalPower;
                if (myTeamPower <= min) {
                    min = myTeamPower;
                }

                if (randomNumber >= min) {
                    result = rival.getName() + " победил";
                } else {
                    result = "Вы победили.";
                }
                result += " " + rivalPower + " " + myTeamPower + " " + randomNumber;
            }
        }
        return result;
    }
    @Transactional
    public int calculatePower (Team team) {
        Integer power = 0;
        Set<Player> players = teamRepository.findPlayersByTeam(team);
        for (Player player : players) {
            power += player.getPerformance();
        }
        power /= players.size();

        return power;
    }
}
