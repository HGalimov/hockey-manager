package org.uennar.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uennar.config.SecurityUtils;
import org.uennar.model.Team;
import org.uennar.model.User;
import org.uennar.repository.TeamRepository;
import org.uennar.service.TeamService;
import org.uennar.util.form.TeamForm;
import org.uennar.util.transformers.TeamFormTransformer;

@Service
@Transactional
public class TeamServiceImpl implements TeamService{
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    TeamFormTransformer transformer;
    @Override
    public void saveTeam(TeamForm form) {
        Team team = transformer.toTeam(form);
        teamRepository.save(team);
    }
}
