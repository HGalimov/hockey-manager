package org.uennar.service;


import org.uennar.util.form.UserForm;

public interface UserService {

    void saveUser(UserForm form);

}
