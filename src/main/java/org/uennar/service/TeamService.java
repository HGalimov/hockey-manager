package org.uennar.service;


import org.uennar.util.form.TeamForm;

public interface TeamService {
    void saveTeam(TeamForm form);
}
